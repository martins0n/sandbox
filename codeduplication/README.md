# Pycode to embeddings


## Parse git repository

```bash
    python parse_project.py --project 'https://github.com/tinkoff-ai/etna'
```

## Create embeddings

```bash
    python create_embeddings.py
```

## TSNE visualization

Notebook `tsne.ipynb` contains example of TSNE visualization of embeddings.